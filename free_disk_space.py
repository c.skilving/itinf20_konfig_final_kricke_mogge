#!/usr/bin/python3
import psutil
import datetime

total, used, free, percent = psutil.disk_usage("/")

total = total // (2**30)
used = used // (2**30)
free = free // (2**30)

WARNING = '\033[91m'
ENDC = '\033[0m'

for disk in psutil.disk_partitions():
        if used < (4 * free):
                print(f"""{datetime.datetime.now()} \n
                diskspace more then 20% left on {disk.device} you have used {percent}% of disk""")
        else:
                print(f"""{datetime.datetime.now()} \n
                {WARNING} + WARNING you dont have enough diskspace + {ENDC} \n
                       you have {free} free GB out of {total} total GB on {disk.device} you have used {percent}% of  Disk """)